#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int example_mtrx_A (float* A, int n, float eps, int num_test) {
    if (num_test == 1 || num_test == 2) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i == j) {
                    A[i * n + j] = 2;
                } else if (i + 1 == j || i == j + 1) {
                    A[i * n + j] = 1;
                } else {
                    A[i * n + j] = 0;
                }
            }
        }
    } else {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i == j) {
                    A[i * n + j] = (float)(2 * (i + 1)) + eps;
                } else if (i + 1 == j || i == j + 1) {
                    A[i * n + j] = 1;
                } else {
                    A[i * n + j] = 0;
                }
            }
        }
    }
    // check on diagonal dominance
    int isOk = 1;
    for (int i = 0; i < n; ++i) {
        if (fabsf(A[i * n + i]) < A[i * n + (i + 1)] + A[i * n + (i - 1)]) {
            isOk = 0;
            break;
        }
    }
    if (isOk) {
        return 1;
    } else {
        return 0;
    }
}

void example_mtrx_f (float* f, int n, float eps, int num_test) {
    if (num_test == 1) {
        for (int i = 0; i < n; ++i) {
            f[i] = 2;
        }
    } else if (num_test == 2) {
        for (int i = 0; i < n; ++i) {
            f[i] = 2 + eps;
        }
    } else {
        for (int i = 0; i < n; ++i) {
            f[i] = (float)(2 * (i + 2)) + eps;
        }
    }
}

void set_alpha (float* alpha, int n, const float* A) {
    for (int i = 0; i < n; ++i) {
        if (i == 0) {
            alpha[i] = (A[i * n + (i + 1)] / A[i * n + i]);
        } else {
            alpha[i] = A[i * n + (i + 1)] / (A[i * n + i] - A[i * n + (i - 1)] * alpha[i - 1]);
        }
    }
}

void set_beta (float* beta, int n, const float* A, const float* f, const float* alpha) {
    for (int i = 0; i < n; ++i) {
        if (i == 0) {
            beta[i] = (f[i] / A[i * n + i]);
        } else {
            beta[i] = (f[i] + A[i * n + (i - 1)] * beta[i - 1]) / (A[i * n + i] - A[i * n + (i - 1)] * alpha[i - 1]);
        }
    }
}

void sweep (float* x, int n, const float* alpha, const float* beta) {
    for (int i = n - 1; i >= 0; --i) {
        if (i == n - 1) {
            x[i] = beta[i];
        } else {
            x[i] = alpha[i] * x[i + 1] + beta[i];
        }
    }
}

/*void print_mtrx (const float* A, int n, int m) {
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            printf("%f ", A[i * m + j]);
        }
        printf("\n");
    }
}*/

int main () {
    printf("Enter number of test: \n");
    int num_test; // 1, 2, 3
    if (scanf("%d", &num_test) != 1) {
        return 1;
    }
    int n; float eps;
    if (num_test == 1) {
        printf("Enter n: \n");
        if (scanf("%d", &n) != 1) {
            return 1;
        }
        eps = 0;
    } else if (num_test == 2) {
        printf("Enter n and eps: \n");
        if (scanf("%d%f", &n, &eps) != 2) {
            return 1;
        }
    } else if (num_test == 3) {
        printf("Enter n and gamma: \n");
        if (scanf("%d%f", &n, &eps) != 2) {
            return 1;
        }
    } else {
        printf("Not correct number of task!\n");
        return 1;
    }
    float* A = malloc(sizeof(float) * (n * n));
    if (!example_mtrx_A(A, n, eps, num_test)) {
        printf("No diagonal dominance in matrix A!\n");
        free(A);
        return 1;
    }
    float* f = malloc(sizeof(float) * n);
    example_mtrx_f(f, n, eps, num_test);
    float* alpha = malloc(sizeof(float) * n);
    set_alpha(alpha, n, A);
    float* beta = malloc(sizeof(float) * n);
    set_beta(beta, n, A, f, alpha);
    float* x = malloc(sizeof(float) * n);
    sweep(x, n, alpha, beta);
    for (int i = 0; i < n; ++i) {
        printf("x%d = %f\n", i + 1, x[i]);
    }
    free(A);
    free(f);
    free(alpha);
    free(beta);
    free(x);
    return 0;
}
