#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double a, b, c;

typedef struct Double_Wrapper {
    double value;
    char is_value;
} Double_Wrapper;

int sign (double number) {
    if (number > 0) {
        return 1;
    } else if (number < 0) {
        return -1;
    } else {
        return 0;
    }
}

double func (double x) {
    return (x * x * x) + a * (x * x) + b * x + c;
}

double dfunc (double x) {
    return 3 * (x * x) + 2 * a * x + b;
}

double ddfunc (double x) {
    return 6 * x + 2 * a;
}

double Dis_dfunc () {
    return a * a - 3 * b;
}

void set_alpha_and_beta (double* alpha, double* beta, double eps) {
    double sqrt_D = sqrt(Dis_dfunc());
    double x1 = (-a - sqrt_D) / 3;
    double x2 = (-a + sqrt_D) / 3;
    if (x2 - x1 > eps) {
        *alpha = x1;
        *beta = x2;
    } else {
        *beta = x1;
        *alpha = x2;
    }
}

// not use for (-inf, inf)
void localize_root (double* interval, double delta, double eps) {
    if (interval[0] == -INFINITY) {
        double begin_p = interval[1];
        while (func(begin_p) > eps) {
            begin_p -= delta;
        }
        interval[0] = begin_p;
        interval[1] = begin_p + delta;
    } else if (interval[1] == INFINITY) {
        double begin_p = interval[0];
        while (func(begin_p) < -eps) {
            begin_p += delta;
        }
        interval[1] = begin_p;
        interval[0] = begin_p - delta;
    } else {
        return;
    }
}

double search_root (const double* interval, double eps, int* p_count_iter) {
    double begin_p = interval[0];
    double end_p = interval[1];
    if (fabs(func(begin_p)) < eps) {
        return begin_p;
    } else if (fabs(func(end_p)) < eps) {
        return end_p;
    }
    int count_iter = 0;
    double medium_p;
    while (end_p - begin_p > eps) {
        medium_p = begin_p + ((end_p - begin_p) / 2);
        if (sign(func(begin_p)) != sign(func(medium_p))) {
            end_p = medium_p;
        } else {
            begin_p = medium_p;
        }
        count_iter++;
    }
    *p_count_iter = count_iter;
    return medium_p;
}

void analysis_func (Double_Wrapper** roots, double delta, double eps, int* p_count_iter) {
    if (Dis_dfunc() < -eps || fabs(Dis_dfunc()) < eps) { // 1 root
        double* interval = malloc(sizeof(double) * 2);
        if (func(0) > eps) {
            interval[0] = -INFINITY;
            interval[1] = 0;
            localize_root(interval, delta, eps);
            roots[0]->value = search_root(interval, eps, p_count_iter);
            roots[0]->is_value = 1;
        } else if (func(0) < -eps) {
            interval[0] = 0;
            interval[1] = INFINITY;
            localize_root(interval, delta, eps);
            roots[0]->value = search_root(interval, eps, p_count_iter);
            roots[0]->is_value = 1;
        } else {
            roots[0]->value = 0.0;
            roots[0]->is_value = 1;
        }
        free(interval);
        printf("%lf\n", fabs(dfunc(roots[0]->value)));
        printf("%lf\n", fabs(ddfunc(roots[0]->value)));

        if (fabs(dfunc(roots[0]->value)) < eps && fabs(ddfunc(roots[0]->value)) < eps) {
            roots[1]->value = roots[0]->value;
            roots[1]->is_value = 1;
            roots[2]->value = roots[0]->value;
            roots[2]->is_value = 1;
        }
    } else if (Dis_dfunc() > eps) { // 1-3 roots
        double alpha, beta;
        set_alpha_and_beta(&alpha, &beta, eps);
        double* interval = malloc(sizeof(double) * 2);
        if (func(beta) > eps) { // 1 root
            interval[0] = -INFINITY;
            interval[1] = alpha;
            localize_root(interval, delta, eps);
            roots[0]->value = search_root(interval, eps, p_count_iter);
            roots[0]->is_value = 1;
        } else if (fabs(func(beta)) < eps && fabs(func(alpha)) < eps) { // 1 root
            roots[0]->value = (alpha + beta) / 2;
            roots[0]->is_value = 1;
        } else if (fabs(func(beta)) < eps) { // 2 roots
            // 1
            interval[0] = -INFINITY;
            interval[1] = alpha;
            localize_root(interval, delta, eps);
            roots[0]->value = search_root(interval, eps, p_count_iter);
            roots[0]->is_value = 1;
            // 2
            roots[1]->value = beta;
            roots[1]->is_value = 1;
            roots[2]->value = beta;
            roots[2]->is_value = 1;
        } else if (func(beta) < -eps && func(alpha) > eps) { // 3 roots
            // 1
            interval[0] = -INFINITY;
            interval[1] = alpha;
            localize_root(interval, delta, eps);
            roots[0]->value = search_root(interval, eps, p_count_iter);
            roots[0]->is_value = 1;
            // 2
            interval[0] = alpha;
            interval[1] = beta;
            roots[1]->value = search_root(interval, eps, p_count_iter);
            roots[1]->is_value = 1;
            // 3
            interval[0] = beta;
            interval[1] = INFINITY;
            localize_root(interval, delta, eps);
            roots[2]->value = search_root(interval, eps, p_count_iter);
            roots[2]->is_value = 1;
        } else if (fabs(func(alpha)) < eps) { // 2 roots
            // 1
            roots[0]->value = alpha;
            roots[0]->is_value = 1;
            roots[1]->value = alpha;
            roots[1]->is_value = 1;
            // 2
            interval[0] = beta;
            interval[1] = INFINITY;
            localize_root(interval, delta, eps);
            roots[2]->value = search_root(interval, eps, p_count_iter);
            roots[2]->is_value = 1;
        } else if (func(alpha) < -eps) { // 1 root
            interval[0] = beta;
            interval[1] = INFINITY;
            localize_root(interval, delta, eps);
            roots[0]->value = search_root(interval, eps, p_count_iter);
            roots[0]->is_value = 1;
        }
        free(interval);
    }
}

int main (void) {
    const double DELTA = 10;
    double eps;
    if (scanf("%lf", &eps) != 1) {
        perror("No data received (epsilon)!\n");
        return 1;
    }
    if (eps <= 0) {
        perror("Incorrect epsilon!\n");
        return 1;
    }
    if (scanf("%lf%lf%lf", &a, &b, &c) != 3) {
        perror("No data received (a, b, c)!\n");
        return 1;
    }

    Double_Wrapper** roots = malloc(sizeof(Double_Wrapper*) * 3);
    for (int i = 0; i < 3; ++i) {
        roots[i] = malloc(sizeof(Double_Wrapper));
        roots[i]->is_value = 0;
    }

    int* count_iter = malloc(sizeof(int));

    analysis_func(roots, DELTA, eps, count_iter);

    printf("count iter = %d\n", *count_iter);

    printf("x1 = %.10lf\n", roots[0]->value);
    if (roots[1]->is_value) {
        printf("x2 = %.10lf\n", roots[1]->value);
    }
    if (roots[2]->is_value) {
        printf("x3 = %.10lf\n", roots[2]->value);
    }

    for (int i = 0; i < 3; ++i) {
        free(roots[i]);
    }
    free(roots);
    return 0;
}
